﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PointsPanel : MonoBehaviour {

    Text txt;
    MainController mc;
    
    void Start()
    {
        txt = GetComponent<Text>();
        mc = FindObjectOfType<MainController>();
    }
     
    void Update()
    {
        
        txt.text = Format(((int)mc.totalPoints).ToString());
    }


    string Format(string s) {
        if (s.Length < 8)
        {
            string n = "";
            for (int i =0; i < 8 - s.Length; i++)
            {
                n += "0";
            }
            n += s;
            return n;
        }
        else
        {
            return s;
        }
    }
}
