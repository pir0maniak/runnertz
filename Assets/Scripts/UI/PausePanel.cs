﻿using UnityEngine;
using System.Collections;

public class PausePanel : MonoBehaviour {

    public GameObject pausePanel;

	public void ActivatePausePanel()
    {
        if (!MainController.gameOver)
        {
            pausePanel.SetActive(true);
            MainController.stop = true;
        }
    }

    public void Continue()
    {
        pausePanel.SetActive(false);
        MainController.stop = false;
    }

    void OnApplicationPause()
    {
        Debug.Log("On pause");
        ActivatePausePanel();
    }

    public void Quit()
    {
        Application.Quit();
    }
}
