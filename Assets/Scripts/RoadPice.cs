﻿using UnityEngine;
using System.Collections;

public class RoadPice : MonoBehaviour {

    private static MainController mc;

    void Start()
    {
        if(mc==null)
        mc = FindObjectOfType<MainController>();
    }

	void Update () {
        if(!MainController.stop && !MainController.gameOver)
        transform.position += new Vector3(0, 0, -mc.speed * Time.deltaTime);
    }
}
