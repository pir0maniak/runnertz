﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MainController : MonoBehaviour {

    public float speed = 1;
    public GameObject roadPicePref;
    List<GameObject> roadPices = new List<GameObject>();
    public static bool stop;
    public static bool gameOver = false;
    public GameObject gameOverPanel;
    public float totalPoints = 0;
    private float speedUp = 30;
    private float tempSpeedUp = 0;

    void Start()
    {
        tempSpeedUp = speedUp;
        gameOver = false;
        stop = false;
        for(int i=0; i < 5; i++)
        {

            GameObject g =  Instantiate(roadPicePref, new Vector3(0, 0, 40 * i), Quaternion.identity) as GameObject;
            roadPices.Add(g);
            if (i > 0)
                g.GetComponent<ObstencleGenerator>().Init();
        }
    }

	// Update is called once per frame
	void Update () {
        if (!stop && !gameOver)
        {
            totalPoints += (speed/5)*Time.deltaTime;
            tempSpeedUp -= Time.deltaTime;
            if (tempSpeedUp <= 0)
            {
                speed++;
                tempSpeedUp = speedUp;
            }
        }
	}

    public void GameOver()
    {
        gameOver = true;
        gameOverPanel.SetActive(true);
    }

    public void Stop()
    {
        stop = true;
    }

    public void AddNewPice()
    {
        Destroy(roadPices[0]);
        roadPices.RemoveAt(0);
        GameObject g = Instantiate(roadPicePref, new Vector3(0, 0, 40 +roadPices[roadPices.Count-1].transform.position.z), Quaternion.identity) as GameObject;
        roadPices.Add(g);
        g.GetComponent<ObstencleGenerator>().Init();
    }
}
