﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObstencleGenerator : MonoBehaviour {

    public int density;
    public GameObject[] obstenclePrefs;
    [SerializeField]
    public List<GameObject> spawnPoints;
    

    public void Init()
    {
        if (density > spawnPoints.Count)
            density = 5;
        for(int i=0; i<density; i++)
        {
            int spawnNum = 0;
            while (true)
            {
                spawnNum = Random.Range(0, spawnPoints.Count);

                if (spawnPoints.FindAll(n => n.transform.position.z == spawnPoints[spawnNum].transform.position.z).Count > 1)
                    break;
                else
                    spawnPoints.Remove(spawnPoints.Find(n => n.transform.position.z == spawnPoints[spawnNum].transform.position.z));
            }
            GameObject g = Instantiate(obstenclePrefs[Random.Range(0, obstenclePrefs.Length)], spawnPoints[spawnNum].transform.position, Quaternion.identity) as GameObject;
            g.transform.SetParent(transform);

            spawnPoints.RemoveAt(spawnNum);
        }
    }


}
